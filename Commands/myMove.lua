function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "oneunit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "x",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "y",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "z",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local what = parameter.oneunit -- unitID
	local x = parameter.x
	local y = parameter.y
	local z = parameter.z
	
	local cx, cy, cz = Spring.GetUnitPosition(what)
	if (cx == nil) or (cz == nil) then
		return FAILURE
	elseif (x + 10 > cx) and (x - 10 < cx) and (z + 10 > cz) and (z - 10 < cz) then
		return SUCCESS
	else 
		Spring.GiveOrderToUnit(what, CMD.MOVE, Vec3(x, y, z):AsSpringVector(), {})
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
